def fix_password(password):
    return password.replace("m", "k").replace("q", "o").replace("g", "e")

password = "sljmai ugrf rfc ambc: lglc dmsp mlc rum"
print(fix_password(password))