def seven_boom(end_number):
    list = []

    for num in range(end_number + 1):
        if num % 7 == 0:
            list.append("BOOM")
        else:
            list.append(num)

    return list

print(seven_boom(17))
