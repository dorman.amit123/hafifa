class animal:
    def __init__(self, name):
        self.name = name
        self.hunger = 0

    def get_name(self):
        return self.name

    def is_hungry(self):
        if self.hunger > 0:
            return True
        else:
            return False

    def feed(self):
        self.hunger -= 1

class dog(animal):
    def __init__(self, name):
        self.name = name
        self.hunger = 10

    def talk(self):
        word = "wof wof"
        print(word)

    def fetch_stick(self):
        print("There you go, sir!")

class cat(animal):
    def __init__(self, name):
        self.name = name
        self.hunger = 3

    def talk(self):
        word = "meow"
        print(word)

    def chase_laser(self):
        print("Meeeeow")

class skunk(animal):
    def __init__(self, name):
        self.name = name
        self.hunger = 0
        self.stink_count = 6

    def talk(self):
        word = "tssss"
        print(word)

    def stink(self):
        print("Dear lord!")

class unicorn(animal):
    def __init__(self, name):
        self.name = name
        self.hunger = 7

    def talk(self):
        word = "Good day, darling"
        print(word)

    def sing(self):
        print("I’m not your toy...")

class dragon(animal):
    def __init__(self, name):
        self.name = name
        self.hunger = 1450
        self.color = "green"

    def talk(self):
        word = "Raaaawr"
        print(word)

    def breath_fire(self):
        print("$@#$#@$")


def main():
    dog1 = dog("brownie")
    cat1 = cat("zelda")
    skunk1 = skunk("stinky")
    unicorn1 = unicorn("keith")
    dragon1 = dragon("lizzy")

    zoo_lst = [dog1, cat1, skunk1, unicorn1, dragon1]

    for animal in zoo_lst:
        while animal.is_hungry():
            animal.feed()

    print(dog1.get_name())
    print(dog1.is_hungry())
    print(dog1.talk())

main()