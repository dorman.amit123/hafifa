def is_prime(number):
    return (True if number % 2 == 0 or number % 3 == 0 or number % 4 == 0 or number % 5 == 0 or number % 6 == 0 or
            number % 7 == 0 or number % 8 == 0 or number % 9 == 0 or number % 10 == 0 else False)

print(bool(is_prime(43)))
print(bool(is_prime(40)))