def numbers_letters_count(my_str):
    num_of_numbers = 0
    num_of_letters = 0

    for char in my_str:
        if char.isalpha() == True:
            num_of_letters += 1
        else:
            num_of_numbers += 1

    return [num_of_numbers, num_of_letters]

print(numbers_letters_count("amit11"))
