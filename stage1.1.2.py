def double_letter(my_str):
    return my_str[::1] + my_str[::1]


def add_letters(s):
    return s[::1]


word = map(add_letters, list(map(double_letter, "amit")))
word = ''.join(list(word))
print(word)
