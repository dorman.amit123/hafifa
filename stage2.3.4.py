class pixel:
    def __init__(self, x=0, y=0, red=0, blue=0, green=0):
        self.x = x
        self.y = y
        self.red = red
        self.blue = blue
        self.green = green

    def set_coords(self, x, y):
        self.x = x
        self.y = y

    def set_grayscale(self):
        self.red = 128
        self.blue = 128
        self.green = 128

    def print_pixel_info(self):
        print("x: " + str(self.x) + " y: " + str(self.y) + " color: " + "(" + str(self.red) + "," +
              str(self.green) + "," + str(self.blue) + ")")

def main():
    pixel1 = pixel(12, 34, 128, 56, 11)
    pixel1.print_pixel_info()

main()


