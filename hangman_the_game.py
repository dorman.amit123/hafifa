start_image = """ 
Welcome to the game Hangman
    _    _
   | |  | |
   | |__| | __ _ _ __   __ _ _ __ ___   __ _ _ __
   |  __  |/ _' | '_ \ / _' | '_ ' _ \ / _' | '_  |
   | |  | | (_| | | | | (_| | | | | | | (_| | | | |
   |_|  |_|\__,_|_| |_|\__, |_| |_| |_|\__,_|_| |_|
                        __/ |
                       |___/"""

image1 = """        
    x-------x
"""

image2 = """        
    x-------x
    |
    |
    |
    |
    |
"""

image3 = """        
    x-------x
    |       |
    |       0
    |
    |
    |
"""

image4 = """        
    x-------x
    |       |
    |       0
    |       |
    |
    |
"""

image5 = """        
    x-------x
    |       |
    |       0
    |      -|-
    |
    |
"""

image6 = """        
    x-------x
    |       |
    |       0
    |      -|-
    |      |
    |
"""

image7 = """        
    x-------x
    |       |
    |       0
    |      -|-
    |      | |
    |
"""

hangman_photos = {1: image1, 2: image2, 3: image3, 4: image4, 5: image5, 6: image6, 7: image7}


def check_win(secret_word, old_letters_guessed):
    """
    this function checks if the player won by checking if all the letters thet contain the
    secret word in the old_letters_guessed list

    :param secret_word: String
    :param old_letters_guessed: list

    :return: boolean
    """
    for letter in secret_word:
        if letter in old_letters_guessed:
            pass
        else:
            return False
    return True


def show_hidden_word(secret_word, old_letters_guessed):
    """
    This function gets a secret_word and a list of letters that the user
    guest and if the letter in the scentens he puts it in new word else
    he puts in the new word "_"

    :param secret_word: String
    :param old_letters_guessed: list

    :return: String
    """
    new_secret_word = ""

    for letter in secret_word:
        if letter in old_letters_guessed:
            new_secret_word += letter
        else:
            new_secret_word += "_ "

    return new_secret_word


def check_valid_input(letter):
    """
    This function gets a letter and return True if the letter is ok
    and else the function will return false

    :param letter: the letter that the user gust

    :return: boolean
    """
    if letter.isalpha() == False:
        return False
    if len(letter) > 1:
        return False
    if len(letter) > 1 and letter.isalpha() == False:
        return False
    if letter.isupper() == True:
        return True
    if letter.islower() == True:
        return True


def try_update_letter_guessed(letter_guessed, old_letters_guessed):
    """
    This function add to the list of the old letters that the user
    gust the new letter if the user didn't guest her before and the
    word is ok

    :param letter_guessed: the letter that the user guest
    :param old_letters_guessed: list of the old letters guest

    :return: boolean
    """
    if check_valid_input(letter_guessed) == True:
        old_letters_guessed += letter_guessed
        print(old_letters_guessed)
        return old_letters_guessed
    else:
        print("X")
        print(old_letters_guessed)
        return old_letters_guessed


def choose_word(file_path, index):
    """
    This function gets a path for a file that contains the words foe the game
    and am index that tells the function witch word to choose

    :param file_path: the path to the file
    :param index: the place of the word in the file

    :return: tuple(int, String)
    """

    file = open(file_path, "r")
    data = file.read()
    list_of_words = data.split()
    file.close()

    secret_word = list_of_words[int(index)]
    len_of_secret_word = len(secret_word)

    return len_of_secret_word, secret_word



def print_photo(stage):
    global hangman_photos
    print(hangman_photos[stage])

def main():
    print(start_image)

    # a list of all the letters that the player guessed
    old_letters_guessed = []
    # your stage in the game
    stage = 1
    path_to_word = input("Enter the path to the words (the path is words.txt): ")
    the_words_number = input("Enter a number 0-3 to choose a word from the file: ")

    len_of_secret_word, secret_word = choose_word("words.txt", the_words_number)

    print_photo(stage)
    print(show_hidden_word(secret_word, old_letters_guessed))

    while stage < 7:
        letter_guessed = input("Guess a letter: ")

        # checks if the letter is OK
        if check_valid_input(letter_guessed) == True:
            if letter_guessed not in old_letters_guessed:
                if letter_guessed in secret_word:
                    old_letters_guessed = try_update_letter_guessed(letter_guessed, old_letters_guessed)
                    print(show_hidden_word(secret_word, old_letters_guessed))
                else:
                    stage += 1
                    old_letters_guessed = try_update_letter_guessed(letter_guessed, old_letters_guessed)
                    print_photo(stage)
            else:
                print("The letter that you have enter is in the list try again")
        else:
            print("Enter a good letter!!!")

        if check_win(secret_word, old_letters_guessed) == True:
            print("YOU WON!!!")
            stage = 8

        print("The letter that you guessed are: " + str(old_letters_guessed))

    if check_win(secret_word, old_letters_guessed) == False:
        print("YOU LOST!!!")
        print("The secret word was: " + secret_word)


if __name__ == "__main__":
    main()