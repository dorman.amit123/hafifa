class dog:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def birthday(self):
        self.age += 1

    def get_age(self):
        return self.age

def main():
    dog1 = dog("dan", 12)
    dog2 = dog("ron", 13)

    dog1.birthday()
    print(dog1.get_age())

main()