def intersection(list1, list2):
    return [x for x in list1 for y in list2 if x == y]


print(intersection([1, 2, 3, 4], [2, 7, 8, 1]))
