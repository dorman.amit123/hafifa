import socket
from datetime import datetime
import random

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.bind(("0.0.0.0", 8820))
server_socket.listen()
print("Server is up and running")
(client_socket, client_address) = server_socket.accept()
print("Client connected")
data = ""


def return_name():
    client_socket.send("my name is server".encode())


def return_time():
    now = datetime.now()
    current_time = now.strftime("%H:%M:%S")

    client_socket.send(("the time is: " + str(current_time)).encode())


def return_random_number():
    number = random.randint(0, 10)
    client_socket.send(("the number is: " + str(number)).encode())


while True:
    data = client_socket.recv(1024).decode()
    if data == "name":
        return_name()
    elif data == "time":
        return_time()
    elif data == "number":
        return_random_number()
    elif data == "quit":
        print("closing client socket now...")
        client_socket.send("bye".encode())
        break
    else:
        client_socket.send(data.encode())

client_socket.close()
server_socket.close()