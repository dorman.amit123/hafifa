import socket
my_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
my_socket.connect(("127.0.0.1", 8820))

data = ""

def recv_message_and_parse(data):
    """
    Parses protocol message and returns command name and data field
    Returns: cmd (str), data (str). If some error occured, returns None, None
    """

    cmd = ""
    msg = ""
    i = 0

    while i < 16:
        if data[i] != "":
            cmd += data[i]

    while i < len(data):
        msg += data[i]

    return cmd, msg


def build_and_send_message(msg_fields):
    """
    Helper method. Gets a list, joins all of it's fields to one string divided by the data delimiter.
    Returns: string that looks like cell1#cell2#cell3
    """

    word = ""

    for field in msg_fields:
        word += field + "#"

    return word


def logout():
    my_socket.send("quit".encode())
    print("Closing client socket")
    my_socket.close()


def main():
    while True:
        try:
            msg = input("Please enter your message\n")
            if msg == "quit":
                logout()
            my_socket.send(msg.encode())
            data = my_socket.recv(1024).decode()
            print(data)
        except:
            exit(0)


if __name__ == "__main__":
    main()
