class BigThing:
    def __init__(self, a):
        self.a = a

    def size(self):
        if isinstance(self.a, int) == True:
            return self.a
        if isinstance(self.a, str) == True:
            return len(self.a)
        if isinstance(self.a, list) == True:
            return len(self.a)

p1 = BigThing("John")

print(p1.size())
