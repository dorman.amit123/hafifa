def four_dividers(number):
    return number % 4 == 0

print(list(filter(four_dividers, range(9))))