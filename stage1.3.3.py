def is_funny(word):
    return word.replace("a", "").replace("h", "") == ""

print(bool(is_funny("hhhahhhahhh")))
print(bool(is_funny("zhhhahhhahhhzzzzaazzzzz")))